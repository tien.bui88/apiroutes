const express = require('express')
      , app = express()

const conf = require('./conf.js')

console.log(`listening on port: ${conf.port}`)

app.get('/', (req, res) => {
  res.status(200).json({msg:`everything is good to go, route is ready`})
})


app.listen(conf.port)